// Show servers internal IPs
output "Internal_IP_of_consul_server_1" {
  value = "${yandex_compute_instance.consul-server1.network_interface.0.ip_address}"
}
output "Internal_IP_of_consul_server_2" {
  value = "${yandex_compute_instance.consul-server2.network_interface.0.ip_address}"
}
output "Internal_IP_of_consul_server_3" {
  value = "${yandex_compute_instance.consul-server3.network_interface.0.ip_address}"
}
output "Internal_IP_of_consul_client_1" {
  value = "${yandex_compute_instance.consul-client1.network_interface.0.ip_address}"
}
output "Internal_IP_of_consul_client_2" {
  value = "${yandex_compute_instance.consul-client2.network_interface.0.ip_address}"
}
output "Internal_IP_of_nginx_server" {
  value = "${yandex_compute_instance.nginx.network_interface.0.ip_address}"
}



// Show servers external IPs
output "External_IP_of_consul_server_1" {
  value = "${yandex_compute_instance.consul-server1.network_interface.0.nat_ip_address}"
}
output "External_IP_of_consul_server_2" {
  value = "${yandex_compute_instance.consul-server2.network_interface.0.nat_ip_address}"
}
output "External_IP_of_consul_server_3" {
  value = "${yandex_compute_instance.consul-server3.network_interface.0.nat_ip_address}"
}
output "External_IP_of_consul_client_1" {
  value = "${yandex_compute_instance.consul-client1.network_interface.0.nat_ip_address}"
}
output "External_IP_of_consul_client_2" {
  value = "${yandex_compute_instance.consul-client2.network_interface.0.nat_ip_address}"
}
output "External_IP_of_nginx_server" {
  value = "${yandex_compute_instance.nginx.network_interface.0.nat_ip_address}"
}