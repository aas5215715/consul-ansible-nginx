// Set variables
variable "yandex_token" {
    description = "Yandex Cloud security OAuth token"
    default     = "" #generate yours by this https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token
    type = string
    sensitive = true
}
variable "yandex_cloud_id" {
    description = "Yandex Cloud ID where resources will be created"
    default     = ""
    type = string
    sensitive = true
}
variable "yandex_folder_id" {
    description = "Yandex Cloud Folder ID where resources will be created"
    default     = ""
    type = string
}
