// Create Consul client instances
resource "yandex_compute_instance" "consul-client1" {
  name = "consul-client1"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  scheduling_policy {
    preemptible = "true"
  }
  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }
  boot_disk {
    initialize_params {
      image_id = "fd8ebb4u1u8mc6fheog1"
      type = "network-hdd"
      size = 15
    }
  }
  network_interface {
    subnet_id = "e9b8pphi6kea6gojfhe1"
    ip_address = "10.128.0.111"
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}
resource "yandex_compute_instance" "consul-client2" {
  name = "consul-client2"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  scheduling_policy {
    preemptible = "true"
  }
  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }
  boot_disk {
    initialize_params {
      image_id = "fd8ebb4u1u8mc6fheog1"
      type = "network-hdd"
      size = 15
    }
  }
  network_interface {
    subnet_id = "e9b8pphi6kea6gojfhe1"
    ip_address = "10.128.0.112"
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}



// Create Consul server instances
resource "yandex_compute_instance" "consul-server1" {
  name = "consul-server1"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  scheduling_policy {
    preemptible = "true"
  }
  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }
  boot_disk {
    initialize_params {
      image_id = "fd8ebb4u1u8mc6fheog1"
      type = "network-hdd"
      size = 15
    }
  }
  network_interface {
    subnet_id = "e9b8pphi6kea6gojfhe1"
    ip_address = "10.128.0.121"
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}
resource "yandex_compute_instance" "consul-server2" {
  name = "consul-server2"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  scheduling_policy {
    preemptible = "true"
  }
  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }
  boot_disk {
    initialize_params {
      image_id = "fd8ebb4u1u8mc6fheog1"
      type = "network-hdd"
      size = 15
    }
  }
  network_interface {
    subnet_id = "e9b8pphi6kea6gojfhe1"
    ip_address = "10.128.0.122"
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}
resource "yandex_compute_instance" "consul-server3" {
  name = "consul-server3"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  scheduling_policy {
    preemptible = "true"
  }
  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }
  boot_disk {
    initialize_params {
      image_id = "fd8ebb4u1u8mc6fheog1"
      type = "network-hdd"
      size = 15
    }
  }
  network_interface {
    subnet_id = "e9b8pphi6kea6gojfhe1"
    ip_address = "10.128.0.123"
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}



// Create nginx instance
resource "yandex_compute_instance" "nginx" {
  name = "nginx"
  folder_id = var.yandex_folder_id
  platform_id = "standard-v2"
  zone = "ru-central1-a"
  scheduling_policy {
    preemptible = "true"
  }
  resources {
    core_fraction = "20"
    cores = 2
    memory = 1
  }
  boot_disk {
    initialize_params {
      image_id = "fd8ebb4u1u8mc6fheog1"
      type = "network-hdd"
      size = 15
    }
  }
  network_interface {
    subnet_id = "e9b8pphi6kea6gojfhe1"
    ip_address = "10.128.0.131"
    nat = true
  }
  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_ed25519.pub")}"
  }
}